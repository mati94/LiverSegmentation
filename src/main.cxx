#include <stdio.h>
#include <iostream>
#include <math.h>
//----------------------------

#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkMinimumMaximumImageFilter.h>
#include <itkIntensityWindowingImageFilter.h>
#include <itkSliceBySliceImageFilter.h>
#include <itkRegionOfInterestImageFilter.h>
#include <itkConnectedThresholdImageFilter.h>
#include <itkConnectedComponentImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkBinaryMorphologicalClosingImageFilter.h>
#include <itkBinaryDilateImageFilter.h>
#include <itkBinaryBallStructuringElement.h>
#include <itkNeighborhoodConnectedImageFilter.h>
#include <itkCurvatureFlowImageFilter.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkExtractImageFilter.h>
#include <itkLabelGeometryImageFilter.h>
#include <itkOrImageFilter.h>
#include <itkRelabelComponentImageFilter.h>

#include <itkMesh.h>
#include <itkBinaryMask3DMeshSource.h>
#include <itkMeshFileWriter.h>
#include <itkLabelOverlapMeasuresImageFilter.h>

//---------------------DEFINICJE TYPÓW--------------------------------------------
typedef itk::Image<signed short, 3> VolumeImageType;
typedef itk::Image<float, 3> FloatVolumeImageType;
typedef itk::Image<signed short, 2> SingleImageType;
typedef itk::Image<float, 2> FloatSingleImageType;
typedef itk::ImageFileReader<VolumeImageType> VolumeReaderType;
typedef itk::ImageFileWriter<VolumeImageType> WriterType;
typedef itk::ImageFileWriter<FloatVolumeImageType> FLWriterType;
typedef itk::SliceBySliceImageFilter<VolumeImageType, VolumeImageType> sliceFilterType;
typedef itk::RegionOfInterestImageFilter< VolumeImageType, VolumeImageType > ROIFilterType;
typedef itk::ExtractImageFilter<VolumeImageType, VolumeImageType> ExtractFilterType;
typedef itk::LabelGeometryImageFilter<VolumeImageType, VolumeImageType> StatisticsFilterType;
typedef itk::BinaryBallStructuringElement<VolumeImageType::PixelType, 2> StructuringElementType;
typedef itk::BinaryBallStructuringElement<VolumeImageType::PixelType, 3> StructuringElement3DType;
typedef itk::BinaryDilateImageFilter<VolumeImageType, VolumeImageType, StructuringElement3DType> dilateImageFilterType;
typedef	itk::NeighborhoodConnectedImageFilter<FloatVolumeImageType, VolumeImageType> ConnectedFilterType;
typedef itk::OrImageFilter<sliceFilterType::InternalInputImageType, sliceFilterType::InternalOutputImageType> orFilterType;
typedef itk::BinaryThresholdImageFilter<VolumeImageType, VolumeImageType> binThreshFilterType;
typedef itk::ConnectedComponentImageFilter<VolumeImageType, VolumeImageType> ConnectedComponentImageFilterType;
typedef itk::RelabelComponentImageFilter <VolumeImageType, VolumeImageType> RelabelFilterType;
typedef itk::IntensityWindowingImageFilter<VolumeImageType, FloatVolumeImageType> IntensityWindowingImageFilterType;
typedef itk::MinimumMaximumImageFilter<VolumeImageType> minFilterType;
typedef itk::CurvatureFlowImageFilter<FloatVolumeImageType, FloatVolumeImageType> CurvatureFlowImageFilterType;
typedef itk::BinaryMorphologicalClosingImageFilter <sliceFilterType::InternalInputImageType, sliceFilterType::InternalOutputImageType, StructuringElementType> closeFilterType;
typedef itk::ConnectedComponentImageFilter<sliceFilterType::InternalInputImageType, sliceFilterType::InternalOutputImageType> connCompFilterType;
typedef itk::RelabelComponentImageFilter<sliceFilterType::InternalInputImageType, sliceFilterType::InternalOutputImageType> relabCompFilterType;
typedef itk::BinaryThresholdImageFilter<VolumeImageType, VolumeImageType> threshFilterType;
typedef itk::Mesh< signed short, 3 > MeshType;
typedef itk::BinaryMask3DMeshSource< VolumeImageType, MeshType > FilterType;
typedef itk::MeshFileWriter< MeshType > Model3DWriterType;

VolumeImageType::Pointer RegionGrowthFromPoints(FloatVolumeImageType::Pointer inputImage, VolumeImageType::IndexType start, VolumeImageType::IndexType end, int numOfPoints) {
	
	VolumeImageType::Pointer outputImage = VolumeImageType::New();
	sliceFilterType::Pointer sliceOrFilter = sliceFilterType::New();

		orFilterType::Pointer orFilter = orFilterType::New();
		sliceOrFilter->SetFilter(orFilter);
		
	VolumeImageType::IndexType point;
	VolumeImageType::PixelType pixel1;
	point[2] = start[2];

	double slope = (double)(start[1] - end[1]) / (double)(start[0] - end[0]);
	double b = start[1] - start[0] * slope;
	double delta = (end[0] - start[0]) / (numOfPoints - 1);
	int firstIteration = 0;
	for (int i = 0; i <= (numOfPoints - 1); i++) {
		point[0] = start[0] + delta*i;
		point[1] = slope*point[0] + b;
		pixel1 = inputImage->GetPixel(point);

		if (pixel1 > 50 && pixel1 < 200) {
			ConnectedFilterType::Pointer connectedThreshold1 = ConnectedFilterType::New();
			connectedThreshold1->SetInput(inputImage);
			connectedThreshold1->SetLower(pixel1 - 30);
			connectedThreshold1->SetUpper(pixel1 + 30);
			itk::Size<3> radius1;
			int rad1 = 3;
			radius1[0] = rad1;
			radius1[1] = rad1;
			radius1[2] = 1;
			connectedThreshold1->SetRadius(radius1);
			connectedThreshold1->SetReplaceValue(1);
			connectedThreshold1->ClearSeeds();
			connectedThreshold1->AddSeed(point);
			connectedThreshold1->Update();

			if (!firstIteration) {
				outputImage = connectedThreshold1->GetOutput();
				firstIteration++;
			}
			else
			{
				sliceOrFilter->SetInput(0, outputImage);
				sliceOrFilter->SetInput(1, connectedThreshold1->GetOutput());
				sliceOrFilter->Update();
				outputImage = sliceOrFilter->GetOutput();
			}


			WriterType::Pointer writerRelabeler = WriterType::New();
			writerRelabeler->SetInput(outputImage);
			//writerRelabeler->SetFileName("./wyniki/watrobaOknoConnThresh.vtk");
			writerRelabeler->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoConnThresh.vtk");
			writerRelabeler->Update();

		}
	}


	return outputImage;
}


int main(int argc, char *argv[])
{
	

	//---------------------ZAŁADOWANIE PLIKU--------------------------------------------
	VolumeReaderType::Pointer volumeReader = VolumeReaderType::New();


	volumeReader->SetFileName("/builds/mati94/LiverSegmentation/PWS-PA000000-ST000000-SE000005_1.dcm");
	//volumeReader->SetFileName("./PWS-PA000005-ST000000-SE000005_1.dcm");
	volumeReader->Update();
	VolumeImageType::Pointer dicomVolume = volumeReader->GetOutput();

	VolumeImageType::SpacingType spacing;
	spacing[0] = 0.8;
	spacing[1] = 0.8;
	spacing[2] = 2.5;
	dicomVolume->SetSpacing(spacing);

	/*VolumeReaderType::Pointer volumeReader2 = VolumeReaderType::New();
	volumeReader2->SetFileName("./wyniki/PWS-PA000015-ST000000-SE000004_1/expert.vtk");
	volumeReader2->Update();

	typedef itk::LabelOverlapMeasuresImageFilter<VolumeImageType> labelOverlapImageFilterType;
	labelOverlapImageFilterType::Pointer labelOverlapImageFilter = labelOverlapImageFilterType::New();
	labelOverlapImageFilter->SetInput(0, volumeReader->GetOutput());
	labelOverlapImageFilter->SetInput(1, volumeReader2->GetOutput());
	labelOverlapImageFilter->Update();

	std::cout << "DICE:" << labelOverlapImageFilter->GetDiceCoefficient() << std::endl;
	std::cout << "JACCARD:" << labelOverlapImageFilter->GetJaccardCoefficient() << std::endl;*/


	//--------------------WYZNACZENIE CENTROIDU I DLUGOSCI OSI GŁOWNYCH------------------------
	
	binThreshFilterType::Pointer binThreshFilter = binThreshFilterType::New();
	binThreshFilter->SetLowerThreshold(-500);
	binThreshFilter->SetInput(dicomVolume);
	binThreshFilter->Update();
	

	VolumeImageType::SizeType binSize = dicomVolume->GetLargestPossibleRegion().GetSize();
	VolumeImageType::IndexType binIndex = dicomVolume->GetLargestPossibleRegion().GetIndex();
	ExtractFilterType::Pointer binSlicefilter = ExtractFilterType::New();
	
	binIndex[2] = round(binSize[2] / 2);
	binSize[2] = 1;

	VolumeImageType::RegionType desiredBinRegion;
	desiredBinRegion.SetSize(binSize);
	desiredBinRegion.SetIndex(binIndex);
	binSlicefilter->SetExtractionRegion(desiredBinRegion);
	binSlicefilter->SetInput(binThreshFilter->GetOutput());
	binSlicefilter->Update();


	
	ConnectedComponentImageFilterType::Pointer connected =
		ConnectedComponentImageFilterType::New();
	connected->SetInput(binSlicefilter->GetOutput());
	connected->SetFullyConnected(false);
	connected->Update();

	
	RelabelFilterType::Pointer relabel =
		RelabelFilterType::New();
	RelabelFilterType::ObjectSizeType minSize = 100;
	relabel->SetInput(connected->GetOutput());
	relabel->Update();

	StatisticsFilterType::Pointer labelStatisticsImageFilter =
		StatisticsFilterType::New();
	labelStatisticsImageFilter->SetInput(relabel->GetOutput());
	labelStatisticsImageFilter->Update();

	WriterType::Pointer writerBin = WriterType::New();
	writerBin->SetInput(relabel->GetOutput());
	//writerBin->SetFileName("./wyniki/watrobaOknoBin.vtk");
	writerBin->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoBin.vtk");
	writerBin->Update();


	itk::Point<double,3> centroid = labelStatisticsImageFilter->GetCentroid(1);
	itk::FixedArray<double, 3> axesLengths = labelStatisticsImageFilter->GetAxesLength(1);

	//---------------------FILTR OKNA INTENSYWNOŚCI--------------------------------------------

	
	IntensityWindowingImageFilterType::Pointer IntensityFilter = IntensityWindowingImageFilterType::New();
	IntensityFilter->SetInput(dicomVolume);
	IntensityFilter->SetWindowMinimum(40);
	IntensityFilter->SetWindowMaximum(140);
	IntensityFilter->SetOutputMinimum(0);
	IntensityFilter->SetOutputMaximum(255);
	IntensityFilter->Update();

	FLWriterType::Pointer writerInt = FLWriterType::New();
	writerInt->SetInput(IntensityFilter->GetOutput());
	//writerInt->SetFileName("./wyniki/watrobaOknoIntesty.vtk");
	writerInt->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoIntesty.vtk");
	writerInt->Update();

	//---------------------WYZNACZANIE ROI VOLUMENU--------------------------------------------
	VolumeImageType::SizeType inSize = dicomVolume->GetLargestPossibleRegion().GetSize();
	ROIFilterType::Pointer ROIfilter = ROIFilterType::New();
	VolumeImageType::IndexType start;
	int width = 50;
	int height = 50;
	start[0] = round(centroid[0]) - width; //Szerokosc
	start[1] = round(centroid[1]);
	start[2] = 0;

	VolumeImageType::SizeType size;
	size[0] = 2* width;
	size[1] = 2* height;
	size[2] = inSize[2];

	VolumeImageType::RegionType desiredRegion;
	desiredRegion.SetSize(size);
	desiredRegion.SetIndex(start);

	ROIfilter->SetRegionOfInterest(desiredRegion);
	ROIfilter->SetInput(dicomVolume);

	WriterType::Pointer writerROI = WriterType::New();
	writerROI->SetInput(ROIfilter->GetOutput());
	//writerROI->SetFileName("./wyniki/watrobaOknoROI.vtk");
	writerROI->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoROI.vtk");
	writerROI->Update();

	//---------------------WYZNACZENIE ROI (WYBRANIE WARSTWY)--------------------------------------------

	ROIFilterType::Pointer ROIfilter2D = ROIFilterType::New();
	VolumeImageType::SizeType inSize2 = ROIfilter->GetOutput()->GetLargestPossibleRegion().GetSize();
	VolumeImageType::RegionType desiredRegion2D;
	VolumeImageType::IndexType start2D;
	start2D[0] = 0; //Szerokosc
	start2D[1] = 0; //Wysokosc
	VolumeImageType::SizeType size2D;
	size2D[0] = inSize2[0];
	size2D[1] = inSize2[1];
	size2D[2] = 1;
	desiredRegion2D.SetSize(size2D);
	ROIfilter2D->SetInput(ROIfilter->GetOutput());

	//---------------------PRZEGLAD PO OSI Z--------------------------------------------
	
	minFilterType::Pointer minFilter = minFilterType::New();
	int firstLungSlice = 0;
	int minimum = 0;
	for (int i = round(inSize2[2] / 2); i < inSize2[2]; i++) {
		start2D[2] = i;
		desiredRegion2D.SetIndex(start2D);
		ROIfilter2D->SetRegionOfInterest(desiredRegion2D);

		minFilter->SetInput(ROIfilter2D->GetOutput());
		minFilter->Update();
		minimum = minFilter->GetMinimum();
		if (minimum < -500) {
			firstLungSlice = i;
			WriterType::Pointer writerROI2 = WriterType::New();
			writerROI2->SetInput(ROIfilter2D->GetOutput());
			//writerROI2->SetFileName("./wyniki/watrobaOknoROImin.vtk");
			writerROI2->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoROImin.vtk");
			writerROI2->Update();
			break;
		}
	}

	//---------------------FILTRACJA ANIZOTROPOWA-------------------------------------------
	
	CurvatureFlowImageFilterType::Pointer curvFilter = CurvatureFlowImageFilterType::New();
	curvFilter->SetNumberOfIterations(40);
	curvFilter->SetTimeStep(0.125);
	curvFilter->SetInput(IntensityFilter->GetOutput());
	curvFilter->Update();

	FLWriterType::Pointer writerSlice = FLWriterType::New();
	writerSlice->SetInput(curvFilter->GetOutput());
	//writerSlice->SetFileName("./wyniki/watrobaOknoCurvFlow.vtk");
	writerSlice->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoCurvFlow.vtk");
	writerSlice->Update();
	
//----------------------------------ROZROST OBSZARU-------------------------------------------------

	VolumeImageType::IndexType seedPoint1;
	seedPoint1[0] = round(centroid[0] - axesLengths[2] / 4);
	seedPoint1[1] = round(centroid[1]);
	seedPoint1[2] = firstLungSlice + 5;
	VolumeImageType::IndexType seedPoint2;
	seedPoint2[0] = round(centroid[0]);
	seedPoint2[1] = round(centroid[1] - axesLengths[1] / 4);
	seedPoint2[2] = firstLungSlice + 5;

	VolumeImageType::Pointer connectedBinImage = RegionGrowthFromPoints(curvFilter->GetOutput(), seedPoint1, seedPoint2, 5);


	//---------------------OPERACJE MORFOLOGICZNE--------------------------------------------
	StructuringElementType structuringElementClose;
	structuringElementClose.SetRadius(20);
	structuringElementClose.CreateStructuringElement();

			//-------------------------------ZAMKNIECIE-----------------------------------------
	sliceFilterType::Pointer sliceCloseFilter = sliceFilterType::New();
	sliceCloseFilter->SetInput(connectedBinImage);

	
	closeFilterType::Pointer closeFilter = closeFilterType::New();
	closeFilter->SetKernel(structuringElementClose);
	closeFilter->SetForegroundValue(1);
	sliceCloseFilter->SetFilter(closeFilter);
	sliceCloseFilter->Update();

	WriterType::Pointer writerClose = WriterType::New();
	writerClose->SetInput(sliceCloseFilter->GetOutput());
	//writerClose->SetFileName("./wyniki/watrobaOknoClose.vtk");
	writerClose->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoClose.vtk");
	writerClose->Update();

	//---------------------ETYKIETOWANIE OSOBNYCH OBIEKTÓW------------------------------------------
	sliceFilterType::Pointer sliceFilter = sliceFilterType::New();
	sliceFilter->SetInput(sliceCloseFilter->GetOutput());

	connCompFilterType::Pointer connCompFilter = connCompFilterType::New();
	connCompFilter->SetFullyConnected(true);
	sliceFilter->SetFilter(connCompFilter);
	sliceFilter->Update();

	//--------------------SORTOWANIE I ZAZNACZENIE OBIEKTÓW WIEKSZYCH OD 600px------------------------------
	sliceFilterType::Pointer sliceFilter2 = sliceFilterType::New();
	sliceFilter2->SetInput(sliceFilter->GetOutput());
	
	relabCompFilterType::Pointer relabCompFilter = relabCompFilterType::New();
	relabCompFilter->SetMinimumObjectSize(600);
	sliceFilter2->SetFilter(relabCompFilter);
	sliceFilter2->Update();
	
	threshFilterType::Pointer threshFilter = threshFilterType::New();
	threshFilter->SetInput(sliceFilter2->GetOutput());
	threshFilter->SetInsideValue(1);
	threshFilter->SetOutsideValue(0);
	threshFilter->SetLowerThreshold(1);
	threshFilter->SetUpperThreshold(2);
	threshFilter->Update();

	StructuringElement3DType structuringElementDilate;
	structuringElementDilate.SetRadius(5);
	structuringElementDilate.CreateStructuringElement();

	dilateImageFilterType::Pointer dilateFilter = dilateImageFilterType::New();
	dilateFilter->SetInput(threshFilter->GetOutput());
	dilateFilter->SetKernel(structuringElementDilate);
	dilateFilter->SetForegroundValue(1);
	dilateFilter->Update();

	WriterType::Pointer writerThresh = WriterType::New();
	writerThresh->SetInput(dilateFilter->GetOutput());
	//writerThresh->SetFileName("./wyniki/watrobaOknoThresh.vtk");
	writerThresh->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaOknoThresh.vtk");
	writerThresh->Update();



	////---------------------MARCHING CUBES--------------------------------------------
	
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput(threshFilter->GetOutput());
	filter->SetObjectValue(1);
	filter->Update();

	//---------------------ZAPIS DO PLIKU OBJ--------------------------------------------
	
	Model3DWriterType::Pointer writer3D = Model3DWriterType::New();
	//writer3D->SetFileName("./wyniki/watrobaModel.obj");
	writer3D->SetFileName("/builds/mati94/LiverSegmentation/wyniki/watrobaModel.obj");
	writer3D->SetInput(filter->GetOutput());
	writer3D->Update();
	return 0;
}

